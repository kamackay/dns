package server

import (
	"github.com/gin-gonic/gin"
	"github.com/miekg/dns"
	"gitlab.com/kamackay/dns/dns_resolver"
	"gitlab.com/kamackay/dns/util"
	"net/http"
	"strings"
	"time"
)

func (this *Server) configureDoh(engine *gin.Engine) {
	engine.GET("/dns-query", func(ctx *gin.Context) {
		start := time.Now().UnixNano()
		queryName := ctx.Query("name")
		typeName := ctx.Query("type")
		if len(queryName) == 0 {
			ctx.String(http.StatusBadRequest, "No Query name sent")
			return
		}

		var qType uint16 = dns.TypeA
		switch strings.ToUpper(typeName) {
		case "A":
			qType = dns.TypeA
		case "AAAA":
			qType = dns.TypeAAAA
		}

		result, err := this.getIp(queryName, qType)
		go func() {
			remoteIp := ctx.Request.RemoteAddr
			ip := strings.Split(remoteIp, ":")[0]
			list, _ := this.requestLog.LoadOrStore(ip, make([]string, 0))
			this.requestLog.Store(ip, unique(append(list.([]string), queryName)))
		}()
		defer func() {
			this.log.Infof("Lookup %s in %s -> %s",
				queryName, util.PrintTimeDiffNow(start), result.Ip)
			this.addMetric(Metric{
				MetricType: "Answer",
				Time:       (time.Now().UnixNano() - start) / NanoConv,
				Ip:         result.Ip,
				Server:     result.Server,
				Blocked:    false,
				Domain:     result.Name,
			})
		}()
		if err == nil {
			ctx.Header("content-type", "application/dns-json")
			ctx.JSON(http.StatusOK, dns_resolver.DohResponse{
				Status: 0,
				TC:     false,
				RD:     true,
				AD:     false,
				CD:     false,
				Question: []dns_resolver.DohQuestion{{
					Name: queryName,
					Type: 1,
				}},
				Answer: []dns_resolver.DohAnswer{{
					Name: queryName,
					Type: 1,
					TTL:  result.Ttl,
					Data: result.Ip,
				}},
			})
		} else {
			ctx.String(http.StatusInternalServerError, "")
		}
	})
}
