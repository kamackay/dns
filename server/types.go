package server

import (
	"github.com/miekg/dns"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/dns/dns_resolver"
	"sync"
)

type Server struct {
	resolver   *dns_resolver.DnsResolver
	domains    sync.Map
	config     *Config
	log        *logrus.Logger
	printMutex *sync.Mutex
	stats      Stats
	cronEngine *cron.Cron
	requestLog sync.Map
	logHook    *LogHook
	logs       *[]*Log
}

type Stats struct {
	Started           int64
	Running           *string        `json:"running"`
	Requests          int64          `json:"requests"`
	LookupRequests    int64          `json:"lookupRequests"`
	CachedRequests    int64          `json:"cachedRequests"`
	BlockedRequests   int64          `json:"blockedRequests"`
	FailedRequests    int64          `json:"failedRequests"`
	LatencyStats      LatencyStats   `json:"latency"`
	UnhandledRequests map[uint16]int `json:"unhandled"`
	Domains           []*Domain      `json:"domains"`
	FailedDomains     []string       `json:"failedDomains"`
	RequestLog        RequestLog     `json:"ipLog"`
	Metrics           []Metric       `json:"metrics"`
	Info              []string       `json:"info"`
	Latencies         []int64        `json:"-"`
}

type LatencyStats struct {
	Average string `json:"average"`
	Median  string `json:"median"`
	Min     string `json:"min"`
	Max     string `json:"max"`
}

func (stats *Stats) AddInfo(info string) {
	if stats.Info == nil {
		stats.Info = make([]string, 0)
	}
	stats.Info = append(stats.Info, info)
}

type Config struct {
	Hosts      map[string]interface{} `json:"hosts"`
	Blocks     map[string]bool        `json:"blocks"`
	DnsServers []string               `json:"servers"`
	NoCache    []string               `json:"noCache"`
	DohServer  *string                `json:"dohServer"`
}

type Domain struct {
	Name          string   `json:"name"`
	Time          int64    `json:"time"`
	Ip            string   `json:"ip"`
	Block         bool     `json:"block"`
	Requests      int64    `json:"requests"`
	Server        string   `json:"server"`
	Type          uint16   `json:"type"`
	Ttl           uint32   `json:"ttl"`
	Managed       bool     `json:"managed"`
	LastRequested int64    `json:"lastRequested"`
	History       []string `json:"-"`
}

type Metric struct {
	MetricType string `json:"type"`
	Time       int64  `json:"time"`
	Ip         string `json:"ip"`
	Server     string `json:"server"`
	Blocked    bool   `json:"blocked"`
	Domain     string `json:"domain"`
}

type RequestLog = map[string][]string

func MakeContextualServer(handler ContextualHandler, context interface{}) *ContextualDnsServer {
	return &ContextualDnsServer{
		handler: &handler,
		context: context,
	}
}

// Handler is implemented by any value that implements ServeDNS.
type ContextualHandler interface {
	ServeDNS(w dns.ResponseWriter, r *dns.Msg, context interface{})
}

type ContextualDnsServer struct {
	handler *ContextualHandler
	context interface{}
}

func (server *ContextualDnsServer) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	(*server.handler).ServeDNS(w, r, server.context)
}
