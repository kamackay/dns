package server

import (
	"github.com/sirupsen/logrus"
)

type LogCallback = func(*Log)

type LogHook struct {
	newLog LogCallback
}

type Log struct {
	message string
}

func Hook(callback LogCallback) LogHook {
	return LogHook{
		newLog: callback,
	}
}

func (hook LogHook) Fire(entry *logrus.Entry) (err error) {
	message, err := entry.String()
	if err != nil {
		return err
	}
	hook.newLog(&Log{
		message: message,
	})
	return nil
}

// Levels are the available logging levels.
func (hook LogHook) Levels() []logrus.Level {
	return logrus.AllLevels
}
