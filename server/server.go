package server

import (
	"errors"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/miekg/dns"
	"github.com/robfig/cron/v3"
	"gitlab.com/kamackay/dns/dns_resolver"
	"gitlab.com/kamackay/dns/logging"
	"gitlab.com/kamackay/dns/util"
	"math"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	// 5 Hours
	MinStaleTime  = 600 * 5
	MaxLogRecords = 5000
)

func (this *Server) lookupInMap(domainName string, requestType uint16) (*Domain, int8) {
	domainInterface, ok := lookupInMapAndUpdate(convertMutexToMap(&this.domains),
		domainName, requestType,
		func(domain *Domain) {
			domain.Requests++
		})
	var domain *Domain
	var result int8
	this.log.Debugf("Using: %+v, %t", domain, ok)
	if ok {
		domain = domainInterface.(*Domain)
		if domain == nil {
			return getFailedDomainObj(domainName), NotFound
		} else if domain.Block {
			// If the domain is blocked, add it to the map so that the next lookup is faster
			this.store(createKey(domainName, requestType), &Domain{
				Name:          domainName,
				Time:          time.Now().UnixNano(),
				Ip:            BlockedIp,
				Block:         true,
				Requests:      1,
				Type:          requestType,
				Ttl:           math.MaxUint32,
				History:       make([]string, 0),
				LastRequested: time.Now().UnixNano(),
			})
		}

		result = getResultFromDomain(domain)
		if !domain.IsExpired() {
			domain.LastRequested = time.Now().UnixNano()
			this.stats.CachedRequests++
			return domain, result
		}
	} else {
		result = NotFound
	}

	if domain == nil {
		return getFailedDomainObj(domainName), result
	}
	this.stats.CachedRequests++
	domain.LastRequested = time.Now().UnixNano()
	return domain, result
}

func (this *Server) store(key string, domain *Domain) {
	oldDomainInterface, ok := this.domains.Load(key)
	if ok {
		// Domain was already in map, update
		oldDomain := oldDomainInterface.(*Domain)
		oldDomain.Requests++
		this.domains.Store(key, oldDomain)
	} else {
		// Domain was not in map, add
		this.domains.Store(key, domain)
	}
}

func (this *Server) getIp(domainName string, qType uint16) (*Domain, error) {
	if this.isNoCacheDomain(domainName) {
		this.log.Debugf("Uncached: %s", domainName)
		return this.resolveIp(domainName, qType, false)
	}
	if this.checkBlock(domainName) {
		this.stats.BlockedRequests++
		this.log.Warnf("Blocking %s", domainName)
		return getBlockedDomainObj(domainName), errors.New("blocked " + domainName)
	}
	address, result := this.lookupInMap(domainName, qType)
	if result == Ok {
		return address, nil
	} else if result == Block || address.Block || address.Ip == BlockedIp {
		this.log.Warnf("Blocking %s", domainName)
		this.stats.BlockedRequests++
		this.addMetric(Metric{
			MetricType: "Block",
			Time:       time.Now().UnixNano() / NanoConv,
			Ip:         BlockedIp,
			Server:     NoServer,
			Blocked:    false,
			Domain:     domainName,
		})
		return getBlockedDomainObj(domainName), errors.New("blocked " + domainName)
	} else {
		return this.resolveIp(domainName, qType, true)
	}
}

func (this *Server) resolveIp(domainName string, qType uint16, store bool) (*Domain, error) {
	if result, err := this.resolver.LookupHost(strings.TrimRight(domainName, "."), qType);
		err != nil || len(result.Ips) == 0 {
		this.stats.FailedRequests++
		this.stats.FailedDomains = unique(append(this.stats.FailedDomains, domainName))
		this.log.Error(err)
		return getFailedDomainObj(domainName), err
	} else {
		answer := result.Ips[0]
		this.log.Infof("Fetched \"%s\" = %s from %s",
			domainName, answer.Address, result.Server)
		domain := &Domain{
			Ip:            answer.Address,
			Ttl:           answer.Ttl,
			Name:          domainName,
			Time:          time.Now().UnixNano(),
			Type:          answer.Type,
			Block:         false,
			Requests:      1,
			Server:        result.Server,
			History:       make([]string, 0),
			LastRequested: time.Now().UnixNano(),
		}
		go func() {
			// Add to cache
			if store {
				this.store(createKey(domain.Name, qType), domain)
			}
			this.stats.LookupRequests++
			this.addMetric(Metric{
				MetricType: "Fetch",
				Time:       0,
				Ip:         answer.Address,
				Server:     result.Server,
				Blocked:    false,
				Domain:     domainName,
			})
		}()
		return domain, nil
	}
}

// Return True if Blocked
func (this *Server) checkBlock(domain string) bool {
	val, ok := lookupBoolInMap(this.config.Blocks, domain)
	return ok && val != nil && *val
}

func (this *Server) ServeDNS(w dns.ResponseWriter, r *dns.Msg, context interface{}) {
	defer w.Close()
	start := time.Now().UnixNano()
	this.stats.Requests++
	defer func() {
		if recovered := recover(); recovered != nil {
			fmt.Println("Recovering from:", r)
			this.stats.FailedRequests++
			this.stats.FailedDomains = unique(append(this.stats.FailedDomains, r.Question[0].Name))
		}

		this.stats.Latencies = append(this.stats.Latencies, time.Now().UnixNano()-start)
	}()
	msg := dns.Msg{}
	msg.SetReply(r)
	defer w.WriteMsg(&msg)
	pool := util.NewThreadPool(uint(math.Min(4, float64(len(r.Question)))))
	for _, question := range r.Question {
		pool.Add(func() {
			this.logQuestion(question, context)
			switch question.Qtype {
			default:
				this.stats.UnhandledRequests[question.Qtype]++
				//answer, err := this.lookupOtherType(question)
				//if err != nil {
				//	this.log.Warnf("%+v", err)
				//} else {
				//	// TODO Answer this question
				this.log.Warnf("Unhandled DNS Request: %+v", question)
				//}
				return
			case 65:
				this.stats.UnhandledRequests[question.Qtype]++
				_, err := this.lookupOtherType(question)
				if err != nil {
					this.log.Warnf("%+v", err)
				} else {
					//for _, answerIp := range answer.Ips {
					//	record := new(dns.HTTPS)
					//	record.Hdr = dns.RR_Header{Name: answerIp.Name, Rrtype: question.Qtype,
					//		Class: question.Qclass, Ttl: answerIp.Ttl}
					//	record.Target = answerIp.Name
					//	record.Value = make([]dns.SVCBKeyValue, 0)
					//
					//	msg.Answer = append(msg.Answer, record)
					//}
					// TODO Answer this question
					this.log.Warnf("Unhandled DNS Request: %+v", question)
				}
				return
			case dns.TypeSRV:
				_, err := this.lookupOtherType(question)
				this.stats.UnhandledRequests[question.Qtype]++
				if err != nil {
					this.log.Warnf("%+v", err)
				} else {
					// TODO Answer this question
					this.log.Warnf("Unhandled DNS Request: %+v", question)
				}
				return
			case dns.TypeA, dns.TypeAAAA:
				msg.Authoritative = true
				domain := question.Name
				this.log.Debugf("Request for IP: %+v", question)
				result, err := this.getIp(domain, question.Qtype)
				go func() {
					remoteIp := w.RemoteAddr().String()
					ip := strings.Split(remoteIp, ":")[0]
					list, _ := this.requestLog.LoadOrStore(ip, make([]string, 0))
					this.requestLog.Store(ip, unique(append(list.([]string), domain)))
				}()
				defer func() {
					this.log.Infof("Lookup %s in %s -> %s",
						domain, util.PrintTimeDiffNow(start), result.Ip)
					this.addMetric(Metric{
						MetricType: "Answer",
						Time:       (time.Now().UnixNano() - start) / NanoConv,
						Ip:         result.Ip,
						Server:     result.Server,
						Blocked:    false,
						Domain:     result.Name,
					})
				}()
				if err == nil {
					if question.Qtype == dns.TypeAAAA {
						msg.Answer = append(msg.Answer, &dns.AAAA{
							Hdr:  dns.RR_Header{Name: domain, Rrtype: question.Qtype, Class: question.Qclass, Ttl: result.Ttl},
							AAAA: net.ParseIP(result.Ip),
						})
					} else if question.Qtype == dns.TypeA {
						msg.Answer = append(msg.Answer, &dns.A{
							Hdr: dns.RR_Header{Name: domain, Rrtype: question.Qtype, Class: question.Qclass, Ttl: result.Ttl},
							A:   net.ParseIP(result.Ip),
						})
					}
				}

			}
		})
	}
	pool.StartAndWait()
}

func NewServer() *Server {
	config, err := readConfig()
	if err != nil {
		fmt.Println("Error Reading the Config", err.Error())
		return nil
	}
	logs := make([]*Log, 0)
	hook := Hook(func(log *Log) {
		newL := append(logs, log)
		l := len(newL)
		if l > MaxLogRecords {
			newL = newL[l-MaxLogRecords : l]
		}
		logs = newL
	})
	client := &Server{
		resolver:   dns_resolver.New(config.DnsServers, config.DohServer),
		config:     config,
		printMutex: &sync.Mutex{},
		log:        logging.GetLogger(hook),
		cronEngine: cron.New(cron.WithSeconds()),
		logHook:    &hook,
		logs:       &logs,
		stats: Stats{
			LookupRequests:    0,
			CachedRequests:    0,
			Domains:           make([]*Domain, 0),
			Started:           time.Now().UnixNano(),
			FailedDomains:     make([]string, 0),
			Metrics:           make([]Metric, 0),
			RequestLog:        make(RequestLog, 0),
			Latencies:         make([]int64, 0),
			LatencyStats:      LatencyStats{},
			UnhandledRequests: make(map[uint16]int),
		},
	}
	convertMapToMutex(config.Hosts).
		Range(func(key, value interface{}) bool {
			client.store(createKey(key, dns.TypeA), &Domain{
				Name:          key.(string),
				Ip:            value.(string),
				Time:          time.Now().UnixNano(),
				Block:         false,
				Type:          dns.TypeA,
				History:       make([]string, 0),
				LastRequested: 0,
				Managed:       true,
			})
			return true
		})
	watcher, err := fsnotify.NewWatcher()
	if err == nil && watcher.Add("/config.json") == nil {
		go func() {
			for {
				select {
				// watch for events
				case _ = <-watcher.Events:
					client.log.Info("Reloading Config File")
					client.loadConfig()
				}
			}
		}()
	}
	return client
}

func DnsServer(port int, net string, server *Server) *dns.Server {
	srv := &dns.Server{Addr: ":" + strconv.Itoa(port), Net: net}
	srv.Handler = MakeContextualServer(server, map[string]interface{}{
		"net": net,
	})
	return srv
}

func (this *Server) loadConfig() {
	newConfig, err := readConfig()
	if err != nil {
		fmt.Println("Error Reading the Config", err.Error())
		return
	} else {
		this.log.Info("Reloading Config File")
	}
	this.config = newConfig
	this.resolver = dns_resolver.New(newConfig.DnsServers, newConfig.DohServer)
	convertMapToMutex(newConfig.Hosts).
		Range(func(key, value interface{}) bool {
			for _, qType := range []uint16{dns.TypeA, dns.TypeAAAA} {
				this.domains.Store(createKey(key.(string), qType), &Domain{
					Name:          key.(string),
					Ip:            value.(string),
					Time:          math.MaxInt64,
					Block:         false,
					Type:          qType,
					Ttl:           math.MaxUint32,
					History:       make([]string, 0),
					LastRequested: 0,
					Managed:       true,
				})
			}

			return true
		})
}

func (this *Server) flushDns() error {
	this.iterateDomains(func(key string, domain *Domain) {
		if !domain.Block {
			// Remove all servers except for the blocked ones
			this.domains.Delete(key)
		}
	})
	this.stats.Requests = 0
	this.stats.Metrics = make([]Metric, 0)
	this.stats.FailedDomains = make([]string, 0)
	this.stats.BlockedRequests = 0
	this.stats.FailedRequests = 0
	this.stats.LookupRequests = 0
	this.stats.CachedRequests = 0
	this.stats.Latencies = make([]int64, 0)

	this.loadConfig()

	return nil
}

func (this *Server) PreStart() {
	this.startRest()
	go func() {
		defer this.cronEngine.Start()
		this.loadConfig()
		time.Sleep(time.Second)
		this.pullBlockList()

		_, _ = this.cronEngine.AddFunc("*/30 * * * * *", func() {
			start := time.Now().UnixNano()
			count := 0
			pool := util.NewThreadPool(4)
			this.iterateDomains(func(host string, domain *Domain) {
				if domain.Block || domain.Ttl == math.MaxUint32 ||
					domain.Requests <= 1 {
					// Domain that does not need to be updated
					return
				}
				if domain.SecondsSinceLastRequest() > uint(math.Min(float64(domain.Ttl*10), MinStaleTime)) {
					this.log.Debugf("Not updating stale domain %s", domain.Name)
					return
				}
				if !domain.IsExpired() {
					this.log.Debugf("Domain %s not expired, ttl = %d", domain.Name, domain.Ttl)
					return
				}
				count++
				pool.Add(func() {
					result, err := this.resolver.LookupHost(strings.TrimRight(domain.Name, "."), domain.Type)
					if err != nil {
						this.log.Warnf("Failed to async lookup %s %+v", domain.Name, err)
						return
					}
					if len(result.Ips) == 0 {
						return
					}
					ip := result.Ips[0]
					if ip.Address != domain.Ip {
						// Update with new IP and new TTL
						this.log.Debugf("Updating domain %s to new IP: %s", domain.Name, ip.Address)
						domain.History = unique(append(domain.History, domain.Ip))
						domain.Ip = ip.Address
						domain.Ttl = ip.Ttl
						domain.Time = time.Now().UnixNano()
						this.domains.Store(host, domain)
					} else {
						this.log.Debugf("Domain %s is still up to date", domain.Name)
					}
				})
			})
			pool.StartAndWait()
			if count > 0 {
				this.log.Infof("Updated %d domains in %s", count,
					util.PrintTimeDiffNow(start))
			}
			this.checkMemoryLevel()
		})

		_, _ = this.cronEngine.AddFunc("* * * * * *", this.calculateLatencyStatistics)
	}()
}
