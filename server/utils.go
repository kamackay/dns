package server

import (
	"encoding/json"
	"errors"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"github.com/miekg/dns"
	"gitlab.com/kamackay/dns/dns_resolver"
	"gitlab.com/kamackay/dns/util"
	"gitlab.com/kamackay/dns/wildcard"
	"io/ioutil"
	"math"
	"net/http"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	Timeout        = 360000
	NanoConv       = 1_000_000
	BlockedIp      = "Blocked!"
	NoServer       = "127.0.0.1"
	Ok        int8 = 0
	Block     int8 = 1
	NotFound  int8 = 2
)

func (this *Server) addMetric(metric Metric) {
	this.stats.Metrics = append(this.stats.Metrics, metric)
}

func (this *Server) logQuestion(question dns.Question, context interface{}) {
	this.log.Infof("Question: %s for %s (%+v)",
		qTypeToString(question.Qtype),
		question.Name,
		context)
}

func (this *Server) checkMemoryLevel() {
	//runtime.GC()
	this.log.Infof("Currently using %d goroutines", runtime.NumGoroutine())
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	mbUsed := bToMb(m.Sys)
	if mbUsed > 400 {
		this.log.Infof("High Memory usage(%v MB), attempt to clear some of the %d records",
			mbUsed,
			len(convertMutexToMap(&this.domains)))
		this.iterateDomains(func(key string, domain *Domain) {
			now := time.Now().UnixNano()
			if domain.Block || domain.Managed {
				return
			}
			if (now-domain.LastRequested)/(NanoConv*1000) > int64(domain.Ttl*20) {
				this.log.Infof("Deleting stale record %s", key)
				this.domains.Delete(key)
			}
		})
	} else {
		this.log.Printf("*** Memory Usage: %v MB", mbUsed)
	}
}

func (this *Server) calculateLatencyStatistics() {
	latencies := this.stats.Latencies
	if len(latencies) == 0 {
		return
	}

	// Average
	var total int64 = 0
	for _, n := range latencies {
		total += n
	}
	this.stats.LatencyStats.Average = util.PrintTimeDifference(int64(float64(total) /
		math.Max(float64(len(latencies)), 1)))

	// Min / Max
	min, max := minMax(latencies)
	this.stats.LatencyStats.Min = util.PrintTimeDifference(min)
	this.stats.LatencyStats.Max = util.PrintTimeDifference(max)

	// Median
	this.stats.LatencyStats.Median = util.PrintTimeDifference(util.Median(latencies))
}

func minMax(array []int64) (int64, int64) {
	max := array[0]
	min := array[0]
	for _, value := range array {
		if max < value {
			max = value
		}
		if min > value {
			min = value
		}
	}
	return min, max
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}

func qTypeToString(qType uint16) string {
	switch qType {
	default:
		return fmt.Sprintf("%d", qType)
	case dns.TypeA:
		return "A"
	case dns.TypeAAAA:
		return "AAAA"
	case dns.TypeMX:
		return "MX"
	case 65:
		return "HTTPS"
	}
}

func createKey(host interface{}, qType uint16) string {
	return host.(string) + strconv.Itoa(int(qType))
}

func convertMapToMutex(slice map[string]interface{}) *sync.Map {
	var mutexMap sync.Map
	for key, value := range slice {
		mutexMap.Store(key, value)
	}
	return &mutexMap
}

func unique(slice []string) []string {
	keys := make(map[string]bool)
	list := make([]string, 0)
	for _, entry := range slice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func convertMutexToMap(mutex *sync.Map) map[string]*Domain {
	slice := make(map[string]*Domain)
	mutex.Range(func(key, value interface{}) bool {
		slice[key.(string)] = value.(*Domain)
		return true
	})
	return slice
}

func convertMutexToStringArrMap(mutex *sync.Map) map[string][]string {
	slice := make(map[string][]string)
	mutex.Range(func(key, value interface{}) bool {
		slice[key.(string)] = value.([]string)
		return true
	})
	return slice
}

func (this *Server) pullBlockList() {
	var list []string
	err := getJson("https://api.keith.sh/ls.json", &list)
	if err == nil {
		this.log.Infof("Pulled %d Servers to Block", len(list))
		for _, server := range list {
			name := fmt.Sprintf("^(.*\\.)?%s\\.$", server)
			for _, qType := range []uint16{dns.TypeA, dns.TypeAAAA} {
				this.domains.Store(createKey(name, qType), &Domain{
					Name:     name,
					Time:     time.Now().UnixNano(),
					Ip:       BlockedIp,
					Block:    true,
					Server:   NoServer,
					Requests: 0,
					Type:     qType,
					Ttl:      math.MaxUint32,
					Managed:  true,
				})
			}
		}
	}
}

func readConfig() (*Config, error) {
	data, err := ioutil.ReadFile("/config.json")
	var config Config
	if err == nil {
		err = jsoniter.Unmarshal(data, &config)
		if err != nil {
			return nil, err
		}
	}
	return &config, nil
}

func getJson(url string, target interface{}) error {
	client := &http.Client{Timeout: 10 * time.Second}
	r, err := client.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func getResultFromDomain(domain *Domain) int8 {
	if domain.Block {
		return Block
	}
	return Ok
}

func lookupInMapAndUpdate(items map[string]*Domain, lookup string, qType uint16, updater func(*Domain)) (interface{}, bool) {
	exact, ok := items[createKey(lookup, qType)]
	if ok && exact.Type == qType {
		updater(exact)
		return exact, true
	}
	for _, val := range items {
		pattern := makeStringRegex(val.Name)
		regex, err := regexp.Compile(pattern)
		if err != nil {
			continue
		}
		if regex.MatchString(lookup) && val.Type == qType {
			updater(val)
			//fmt.Printf("Found Match for %s: %s\n", lookup, key)
			return val, true
		}
	}
	return nil, false
}
func lookupBoolInMap(items map[string]bool, lookup string) (*bool, bool) {
	exact, ok := items[lookup]
	if ok {
		return &exact, true
	}
	for key, val := range items {
		if wildcard.Match(key, lookup) {
			return &val, true
		}
	}
	return nil, false
}

func getBlockedDomainObj(domainName string) *Domain {
	return &Domain{
		Ip:       BlockedIp,
		Name:     domainName,
		Time:     time.Now().UnixNano(),
		Block:    true,
		Requests: 1,
		Server:   NoServer,
	}
}

func getFailedDomainObj(domainName string) *Domain {
	return &Domain{
		Ip:       "",
		Name:     domainName,
		Time:     time.Now().UnixNano(),
		Block:    false,
		Requests: 1,
		Server:   NoServer,
	}
}

func (this *Server) lookupOtherType(question dns.Question) (*dns_resolver.DnsResult, error) {
	dohResponse := this.resolver.LookupHostDoh(question.Name, qTypeToString(question.Qtype))
	if dohResponse == nil {
		return nil, errors.New("could not resolve")
	}
	return &dns_resolver.DnsResult{
		Ips:    dohResponse.Ips,
		Server: dohResponse.Server,
	}, nil
}

func (this *Server) iterateDomains(action func(string, *Domain)) {
	this.domains.Range(func(key, value interface{}) bool {
		domain := value.(*Domain)
		host := key.(string)
		action(host, domain)
		return true
	})
}

func (this *Domain) IsExpired() bool {
	return time.Now().UnixNano()/NanoConv-this.Time/NanoConv > int64(this.Ttl)*1000
}

func (this *Domain) SecondsSinceLastRequest() uint {
	return uint((time.Now().UnixNano() - this.LastRequested) / (NanoConv * 1000))
}

func (this *Server) GetMessages() []string {
	logs := make([]string, len(*this.logs))
	for _, log := range *this.logs {
		message := strings.TrimSpace(log.message)
		if len(message) > 0 {
			logs = append(logs, message)
		}
	}
	return logs
}

func makeStringRegex(s string) string {
	pattern := s
	if !strings.HasPrefix(pattern, "^") {
		// Format to be a regex for usage
		pattern = fmt.Sprintf("^%s$", pattern)
	}

	return pattern
}

func (this *Server) isNoCacheDomain(domain string) bool {
	for _, pattern := range this.config.NoCache {
		if pattern == domain || wildcard.Match(pattern, domain) {
			return true
		}
	}
	return false
}
