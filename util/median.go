package util

import "sort"

type ArrType = int64

func Median(sliceList []ArrType) ArrType {
	length := len(sliceList)
	sort.Slice(sliceList, func(i, j int) bool {
		return sliceList[i] < sliceList[j]
	})

	if len(sliceList)%2 == 0 {
		// Even
		return sliceList[(length-1)/2]
	} else {
		// Odd
		return (sliceList[length/2] + sliceList[(length/2)-1]) / 2
	}
}
