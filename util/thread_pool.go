package util

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/dns/logging"
	"sync"
	"time"
)

type ThreadPool struct {
	threadCount uint
	log         *logrus.Logger
	wg          sync.WaitGroup
	jobs        []func()
	start       int64
}

func NewThreadPool(threads uint) ThreadPool {
	return ThreadPool{
		threadCount: threads,
		wg:          sync.WaitGroup{},
		jobs:        make([]func(), 0),
		log:         logging.GetLogger(new(logging.FileHook)),
	}
}

func (pool *ThreadPool) Add(job func()) {
	pool.jobs = append(pool.jobs, job)
}

func (pool *ThreadPool) StartAndWait() {
	pool.log.Debugf("Starting Thread Pool Size %d with %d jobs",
		pool.threadCount, len(pool.jobs))
	pool.start = time.Now().UnixNano()
	numJobs := len(pool.jobs)
	jobs := make(chan int, numJobs)
	results := make(chan bool, numJobs)
	for x := 1; x <= int(pool.threadCount); x++ {
		go pool.doJobs(x, jobs, results)
	}
	for i := 0; i < numJobs; i++ {
		jobs <- i
	}
	close(jobs)
	for i := 0; i < numJobs; i++ {
		<-results
	}
}

func (pool *ThreadPool) doJobs(id int, jobs <-chan int, results chan<- bool) {
	for jobId := range jobs {
		pool.log.Debugf("Runner %d running job %d (%d after starting)\n", id, jobId,
			(time.Now().UnixNano()-pool.start)/1_000_000)
		pool.runJob(pool.jobs[jobId])
		results <- true
	}
}

func (pool *ThreadPool) runJob(job func()) {
	defer func() {
		if r := recover(); r != nil {
			pool.log.Warnf("Recovering from failed Job: %+v", r)
		}
	}()
	job()
}
