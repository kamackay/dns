package logging

import (
	"github.com/sirupsen/logrus"
)

func (hook *FileHook) Fire(entry *logrus.Entry) (err error) {
	//levelInt := parseSeverity(entry.Level.String())
	//message, err := entry.String()
	//if err != nil {
	//	return err
	//}
	return nil
}

type FileHook struct {
}

// Levels are the available logging levels.
func (hook *FileHook) Levels() []logrus.Level {
	return logrus.AllLevels
}
