package main

import (
	"gitlab.com/kamackay/dns/logging"
	"gitlab.com/kamackay/dns/server"
	"gitlab.com/kamackay/dns/util"
)

func main() {
	logger := logging.GetLogger()
	logger.Infof("Starting...")
	dnsServer := server.NewServer()
	port := 53
	nets := []string{"udp", "tcp"}
	pool := util.NewThreadPool(uint(len(nets)))
	dnsServer.PreStart()
	for _, net := range nets {
		dns := server.DnsServer(port, net, dnsServer)
		logger.Infof("Binding to %d/%s", port, net)
		pool.Add(func() {
			if err := dns.ListenAndServe(); err != nil {
				logger.Fatalf("Failed to set %s listener %s\n",
					dns.Net,
					err.Error())
			} else {
				logger.Infof("Started on Port %d", port)
			}
		})
	}
	pool.StartAndWait()
}
